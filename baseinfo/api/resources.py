__author__ = 'bisonchen'

from tastypie.resources import ModelResource
from tastypie.authorization import Authorization
from baseinfo.models import *

class SchoolResource(ModelResource):
    class Meta:
        queryset = School.objects.all()
        authorization = Authorization()
        list_allowed_methods = ['get', 'post']
        detail_allowed_methods = ['get']

class ParticipantResource(ModelResource):
    class Meta:
        queryset = Participant.objects.all()
        authorization = Authorization()
        list_allowed_methods = ['get', 'post','put','delete']
        detail_allowed_methods = ['get', 'post','put','delete']

class CourseResource(ModelResource):
    class Meta:
        queryset = Course.objects.all()
        authorization = Authorization()
        list_allowed_methods = ['get', 'post']
        detail_allowed_methods = ['get']

        filtering = {
            "course_id": ('exact',)
        }
