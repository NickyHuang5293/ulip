# -*- coding: utf-8 -*-
from django.db import models

PARTICIPANT_TYPE=(
    (u'Teacher',u'教师'),
    (u'Student',u'学生'),
    )

# Create your models here.
class School(models.Model):
    sid = models.AutoField(primary_key=True)
    sname = models.CharField(max_length=80)
    isvalid = models.BooleanField(default=1)

class Participant(models.Model):
    pid = models.AutoField(primary_key=True)
    username = models.CharField(max_length=30)
    email = models.EmailField()
    school = models.ForeignKey(School)
    type = models.CharField(max_length=20, choices=PARTICIPANT_TYPE)
    isvalid = models.BooleanField(default=1)

class Course(models.Model):
    course_id = models.AutoField(primary_key=True)
    course_number = models.CharField(max_length=80, verbose_name="课程编号")
    school = models.ForeignKey(School)
    department = models.CharField(max_length=80, verbose_name="开课学院")
    isvalid = models.BooleanField(default=1)
    creator = models.ForeignKey(Participant,verbose_name="开课者",related_name='creator')
    participant = models.ManyToManyField(Participant, through='C_P')


class C_P(models.Model):
    id = models.AutoField(primary_key=True)
    course = models.ForeignKey(Course)
    participant = models.ForeignKey(Participant)
    role = models.CharField(max_length=20, choices=PARTICIPANT_TYPE)
    isdeleted = models.BooleanField(default=False)