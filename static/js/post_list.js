/**
 * Created by PyCharm.
 * User: Administrator
 * Date: 12-5-3
 * Time: 下午9:10
 * To change this template use File | Settings | File Templates.
 */
(function(){

    window.Post = Backbone.Model.extend({
        urlRoot: POST_API,
        idAttribute:'post_id'
    });

    window.Comment = Backbone.Model.extend({
        urlRoot: COMMENT_API,
        idAttribute:'comment_id'
    });

    window.Posts = PaginatedCollection.extend({
        model: Post,
        urlRoot: POST_API,

        // properies needed for PaginatedCollection
        baseUrl:POST_API,
        limit: 10,
        filter_options : {"course__course_id":1},
        sort_field : "-ctime",

//        comparator: function(post){
//            return post.get("ctime");
//        },

        maybeFetch : function(options){
            if(this._fetched){
                options.success && options.success();
                return;
            }
            var self = this;
            var successWrapper = function(success){
                return function(){
                    self._fetched = true;
                    success && success.apply(this,arguments);
                };
            };
            options.success = successWrapper(options.success);
            this.fetch(options);
        }

    });

    window.Comments = Backbone.Collection.extend({
        model: Comment,
        urlRoot: COMMENT_API,

        maybeFetch : function(options){
            if(this._fetched){
                options.success && options.success();
                return;
            }
            var self = this;
            var successWrapper = function(success){
                return function(){
                    self._fetched = true;
                    success && success.apply(this,arguments);
                };
            };
            options.success = successWrapper(options.success);
            this.fetch(options);
        }
    });

    window.PostView = Backbone.View.extend({
        tagName: 'div',
        className: 'postAbs',

        events: {
            'click .delete': 'clear',
            'click .show-comment' :'show_comment',
            'click .comment-submit': 'add_comment'
        },

        initialize: function(){
            this.model.bind('change', this.change_view, this);
            this.model.bind('destroy', this.remove, this);

            $(this.el).data("comment_state","hide");

            this.comment_collection = new Comments();
        },

        change_view: function(){
            this.$('.comment-num').html(this.model.get("comment_count"));
        },

        clear: function(e){
            e.preventDefault();
           this.model.destroy({
               success: function(){ },
               error: function(){ alert("delete failed!"); },
               wait:true
           });
        },

        showCommentPanel: function(){
            this.$(".comment-panel").slideDown();
        },

        hideCommentPanel: function(){
           this.$(".comment-panel").slideUp();
        },

        show_comment: function(e){
            e.preventDefault();
            if($(this.el).data("comment_state")=="hide"){
                this.showCommentPanel();
                $(this.el).data("comment_state","show");

                var view = new CommentSlideDownView({
                    el: this.$('.comment-panel'),
                    collection: this.comment_collection,
                    model: this.model
                });
                view.setPostPt(this);
                view.render(this.model);
                this.comment_collection.fetch({
                     fail: function(){alert("failed to load comments");},
                     data: {"post__post_id": this.model.id}
                 });

            }
            else{
                this.hideCommentPanel();
                $(this.el).data("comment_state","hide");
            }
        },

        add_comment: function(){
            var content = this.$('#new-comment-content').val();
            if(content){
                // add new comment
                post_list_app.comments.create({
                    school: SCHOOL,
                    course: COURSE,
                    creator: PARTICIPANT,
                    content: content,
                    post: POST_API + this.model.id +"/",
                    isdeleted : false
                },{ wait: true});
                this.$('#new-comment-content').val('');

               // update the comment-count
                this.model.save(
                    {comment_count: this.model.get("comment_count") + 1} ,
                    {error: function(){alert("fail update")}}
                );
            }
        },
        render: function(){
            $(this.el).html(ich.postAbsTemplate(this.model.toJSON()));
            return this;
        }
    });

    window.CommentView = Backbone.View.extend({
        tagName: 'div',
        className: 'comment',

        events:{
            'click .delete-comment': 'clear'
        },

        initialize: function(){
            this.model.bind('change', this.render, this);
            this.model.bind('destroy', this.remove, this);
        },

        setPost: function(post){
            this.post = post;
        },

        clear: function(e){
            //window.alert('deleting comment');
            e.preventDefault();
            this.model.destroy({
                error: function(){ alert("delete failed!"); },
                wait:true
            });

            // update the comment-count
            this.post.save(
                {comment_count: this.post.get("comment_count") - 1},
                {error: function(){ alert("fail update"); }}
            );
        },

        render: function(){
            $(this.el).html(ich.commentTemplate(this.model.toJSON()));
            return this;
        }
    });

    window.PostListView = Backbone.View.extend({
        initialize: function(){
            _.bindAll(this, 'addOne', 'appendOne','addAll');

            this.collection.bind('add', this.addOne);
            this.collection.bind('reset', this.addAll, this);
            //$(window).bind('scroll', this.collection, this.loadMore);
            this.views = [];
        },

        addAll: function(){
            this.views = [];  //view.remove()清除
            this.collection.each(this.appendOne);
        },

        //used when fetch old posts
        appendOne: function(post){
            var view = new PostView({
                model: post
            });
            $(this.el).append(view.render().el);
            this.views.push(view);
            view.bind('all', this.rethrow, this);
        },

        //used when create new post
        addOne: function(post){
            var view = new PostView({
                model: post
            });
            $(this.el).prepend(view.render().el);
            this.views.push(view);
            view.bind('all', this.rethrow, this);
        },

        rethrow: function(){
            this.trigger.apply(this, arguments);
        }
    });


    window.EditorView = Backbone.View.extend({
        events:{
            'click .b-submit': 'createPost'
        },

        createPost: function(){
            //var content = this.$('#textContent').val();
            var content = this.$('#textContent').editorBox("get_code");
            if(content){
                this.collection.create({
                    school: SCHOOL,
                    course: COURSE,
                    creator: PARTICIPANT,
                    content: content,
                    comment_count : 0,
                    isdeleted : false
                },{ wait: true});
                this.$('#textContent').val('');
            }
        },

        render: function(){
          $(this.el).html(ich.postEditor());
        }
    });

    window.CommentSlideDownView = Backbone.View.extend({
       events:{
           'click .comment-submit': 'addComment',
           'click .view-all': 'view_all_comments'
       },

       initialize: function(){
           _.bindAll(this, 'addOne', 'addAll');

           this.collection.bind('add', this.addOne);
           this.collection.bind('reset', this.addAll, this);

           this.views = [];
           this.postpt = null;
       },

       addAll: function(){
           this.views = [];
           this.$('.comments').html("");
           this.collection.each(this.addOne);
       },

       addOne: function(comment){
           var view = new CommentView({
               model: comment
           });
           view.setPost(this.post);
           this.$(".comments").prepend(view.render().el);
           this.views.push(view);
           view.bind('all', this.rethrow, this);
       },

        //set PostView pointer(this)
       setPostPt: function(pt){
           this.postpt = pt;
       },

       view_all_comments: function(e){
           e.preventDefault();
           //popup window
           $.blockUI({
                 message: $('.view-all-panel'),    //pop up element
                 css: {    //element css
                     textAlign:'left',
                     top: '50%',
                     left: '50%',
                     marginLeft: '-350px',
                     marginTop: '-250px',
                     background: 'transparent',
                     width: '700px',
                     height: '500px',
                     cursor: 'arrow',
                     border: 'none'
                 },
                 overlayCSS:  {
                     cursor:'arrow'
                 }
             });
           $('.blockOverlay').click($.unblockUI);   //close when click on overlay
           $('.view-all-panel #close').click($.unblockUI);  //close when click on close-button
           $('.view-all-panel .popup-comment').click(function(e){e.stopPropagation();});
           $('.view-all-panel').click( $.unblockUI);

           //hide comment slide down panel
           $(this.el).hide();
           $(this.postpt.el).data("comment_state","hide");

           post_list_app.popupComment.render( this.model);
           post_list_app.comments.fetch({
                fail: function(){alert("fail");},
                data: {"post__post_id": this.model.id}
            });
       },

       addComment: function(){
           var content = this.$('#new-comment-content').val();
           if(content){
               // add new comment
               this.collection.create({
                   school: SCHOOL,
                   course: COURSE,
                   creator: PARTICIPANT,
                   content: content,
                   post: POST_API + this.post.id +"/",
                   isdeleted : false
               },{ wait: true});
               this.$('#new-comment-content').val('');

               // update the comment-count
               this.post.save(
                   {comment_count: this.post.get("comment_count") + 1} ,
                   {error: function(){alert("fail update")}}
               );
           }
       },

       render: function(post){
           this.post = post;
           $(this.el).html(ich.commentSlideDown(post.toJSON()));
       }
   });

    window.CommentPopupView = Backbone.View.extend({
        events:{
            'click .headAction': 'closePopup',
            'click .comment-submit': 'addComment'
        },

        initialize: function(){
            _.bindAll(this, 'addOne', 'addAll');

            this.collection.bind('add', this.addOne);
            this.collection.bind('reset', this.addAll, this);

            this.views = [];
        },

        addAll: function(){
            this.views = [];
            this.collection.each(this.addOne);
        },

        addOne: function(comment){
            var view = new CommentView({
                model: comment
            });
            view.setPost(this.post);
            this.$(".comments").prepend(view.render().el);
            this.views.push(view);
            view.bind('all', this.rethrow, this);
        },

        closePopup: function(){
          $(this.el).hide();
        },

        addComment: function(){
            var content = this.$('#new-comment-content').val();
            if(content){
                // add new comment
                this.collection.create({
                    school: SCHOOL,
                    course: COURSE,
                    creator: PARTICIPANT,
                    content: content,
                    post: POST_API + this.post.id +"/",
                    isdeleted : false
                },{ wait: true});
                this.$('#new-comment-content').val('');

                // update the comment-count
                this.post.save(
                    {comment_count: this.post.get("comment_count") + 1} ,
                    {error: function(){alert("fail update")}}
                );
            }
        },

        render: function(post){
            this.post = post;
            $(this.el).html(ich.commentPopup(this.post.toJSON()));

            //this.postDetailView.render();
        }
    });

    window.PostDetailView = Backbone.View.extend({
        render: function(){
            $(this.el).html(ich.postDetail(this.model.toJSON()));
            return this;
        }
    });

    window.PostPaginatedCollection = PaginatedCollection.extend({
        baseUrl : "/api/v1/post/"
    });

    $(function(){
        window.post_list_app =   window.post_list_app || {};

        post_list_app.posts = new Posts();
        post_list_app.comments = new Comments();

        //initialize the post editor view
        post_list_app.editor = new EditorView({
            el: $("#composer"),
            collection: post_list_app.posts
        });
        post_list_app.editor.render();

        //initialize the post list view
        post_list_app.postlist = new PostListView({
            el:$(".postArea"),
            collection: post_list_app.posts
        });

        post_list_app.posts.maybeFetch({
            success: _.bind(post_list_app.postlist.render, post_list_app.postlist)
            //data: {"course__course_id":1,"order_by":"-ctime"}
        });

        //initialize the popup view, but do not show it
        post_list_app.popupComment = new CommentPopupView({
            el:$(".view-all-panel .popup-comment"),
            collection: post_list_app.comments
        });

        //$("#textContent").val("aaaaa");
        require([
            'tinymce.editor_box'
        ], function() {
            $("#textContent").editorBox();
        });

    });
})();