/**
 * Created by PyCharm.
 * User: Administrator
 * Date: 12-5-3
 * Time: 下午9:10
 * To change this template use File | Settings | File Templates.
 */


    (function(){
    /****************************************BOOK RESOURCE START********************************/

    //define model "ResBook"
    window.SyllabusModel = Backbone.Model.extend({
        urlRoot: SYLLABUS_API,
        idAttribute:'syllabus_id'
    });

    window.SyllabusCollection = Backbone.Collection.extend({
        model: SyllabusModel,
        urlRoot: SYLLABUS_API,

        maybeFetch : function(options){
            if(this._fetched){
                options.success && options.success();
                return;
            }
            var self = this;
            var successWrapper = function(success){
                return function(){
                    self._fetched = true;
                    success && success.apply(this,arguments);
                };
            };
            options.success = successWrapper(options.success);
            this.fetch(options);
        }

    });

    window.SyllabusUnitView = Backbone.View.extend({
        tagName: 'div',
        className: 'unit-block',

        events: {
            'click .delete-syllabus': 'clear',
            'click .edit-syllabus': 'edit',
            'click .edit-syllabus-panel .save-modify': 'save_modify',
            'click .edit-syllabus-panel .cancel-modify': 'cancel_modify',
            'click .syllabus-info': 'detail_popoup'
        },

        initialize: function(){
            this.model.bind('change', this.render, this);
            this.model.bind('destroy', this.remove, this);
        },

        clear: function(e){
            e.preventDefault();
           this.model.destroy({
               success: function(){ },
               error: function(){ alert("delete failed!"); },
               wait:true
           });
        },

        showSyllabusPanel: function(){
            this.$(".syllabus-unit").hide();
            this.$(".edit-syllabus-panel").show();
        },

        hideSyllabusPanel: function(){
            this.$(".edit-syllabus-panel").hide();
            this.$(".syllabus-unit").show();
        },

        edit: function(e){
            e.preventDefault();
            //init editor value
            this.$('.edit-syllabus-panel #title').val(this.model.get('title'));
            this.$('.edit-syllabus-panel #textContent').val(this.model.get('content'));

            this.showSyllabusPanel();
            e.stopPropagation();
        },

        save_modify: function(){
            var obj = {};

            obj['title'] = this.$('#title').val();
            obj['content'] = this.$('#textContent').val();
            this.model.save(obj);

            this.hideSyllabusPanel();
        },

        cancel_modify: function(){
            this.hideSyllabusPanel();
        },

        detail_popoup: function(e){
            e.preventDefault();
        },

        render: function(){
            var data = this.model.toJSON();
            data.content = textToPageEncode(data.content);

            $(this.el).html(ich.SyllabusUnit(this.model.toJSON()));
            this.$('.syllabus-unit .syllabus-content').html(data.content);
            return this;
        }
    });

    window.SyllabusListView = Backbone.View.extend({

        initialize: function(){
            _.bindAll(this, 'addOne', 'addAll');

            this.collection.bind('add', this.addOne);
            this.collection.bind('reset', this.addAll, this);
            this.views = [];
        },

        addAll: function(){
            this.views = [];
            this.collection.each(this.addOne);
        },

        addOne: function(syllabusmodel){

            var view = new SyllabusUnitView({
                model: syllabusmodel
            });
            $(this.el).prepend(view.render().el);
            this.views.push(view);
            view.bind('all', this.rethrow, this);
        },

        rethrow: function(){
            this.trigger.apply(this, arguments);
        }
    });

    window.SyllabusEditorView = Backbone.View.extend({
        events:{
            'click .syllabus-text-opt': 'showSyllabusPanel',
            'click .new-syllabus-panel .cancel' : 'hideSyllabusPanel',
            'click .new-syllabus-panel .save': 'createSyllabus'
//            'click .syllabus-file-opt': 'upload_file'
        },

        showSyllabusPanel: function(){
            this.resetSyllabus();
            this.$(".type-selector").hide();
            this.$(".new-syllabus-panel").slideDown();
        },

        hideSyllabusPanel: function(){
            this.$(".new-syllabus-panel").slideUp();
            this.$(".type-selector").show();
        },

        createSyllabus: function(){
            var title = this.$('.new-syllabus-panel #title').val();
            var content = this.$('.new-syllabus-panel #textContent').val();

            if(title){
                syllabus_list_app.syllabus_collection.create({
                    school: SCHOOL,
                    course: COURSE,
                    creator: PARTICIPANT,
                    isdeleted : false,

                    title: title,
                    content: content

                },{ wait: true});

                this.hideSyllabusPanel();
            }
            else{
                alert("请输入标题！");
            }
        },

        resetSyllabus: function(){
            this.$('.new-syllabus-panel #title').val("");
            this.$('.new-syllabus-panel #textContent').val("");
        },

        upload_file: function(e){
            e.preventDefault();
            this.$('#upload_file').click();
            e.stopPropagation();
        },

        render: function(){
            $(this.el).html(ich.SyllabusEditor());
        }
    });


    $(function(){
        window.syllabus_list_app =   window.syllabus_list_app || {};

        syllabus_list_app.syllabus_collection = new SyllabusCollection();

        //initialize the post list view
        syllabus_list_app.syllabuslist = new SyllabusListView({
            el:$(".syllabus-list"),
            collection: syllabus_list_app.syllabus_collection
        });

        syllabus_list_app.syllabus_collection.maybeFetch({
            success: _.bind(syllabus_list_app.syllabuslist.render, syllabus_list_app.syllabuslist),
            data: {"course__course_id":1}
        });

        var newEditor = new SyllabusEditorView({
            el:$(".add_operation")
        });
        newEditor.render();
    });
})();

