/**
 * Created by PyCharm.
 * User: Administrator
 * Date: 12-6-6
 * Time: 下午3:30
 * To change this template use File | Settings | File Templates.
 */

//页面显示用
function textToPageEncode(fString){
    fString = fString.replace(">", "&gt;");
    fString = fString.replace("<", "&lt;");
    fString = fString.replace(" ", "&nbsp;");
    fString = fString.replace(/\r\n/g, "</p><p>");
    fString = fString.replace(/\n/g, "<br>");
    return fString;
}

//textarea显示用
function pageToTextEncode(fString){
    fString = fString.replace("&gt;", ">");
    fString = fString.replace("&lt;", "<");
    fString = fString.replace("&nbsp;", " ");
    fString = fString.replace("</p><p>", "\r\n");
    fString = fString.replace("<br>", "\n");
    return fString;
}
