/**
 * Created by PyCharm.
 * User: Administrator
 * Date: 12-5-30
 * Time: 下午5:50
 * To change this template use File | Settings | File Templates.
 */

(function(){

    window.Assignment = Backbone.Model.extend({
        urlRoot: ASSIGNMENT_API,
        idAttribute:'assignment_id'
    });

    window.AssignmentList = PaginatedCollection.extend({
        model: Assignment,
        urlRoot: ASSIGNMENT_API,

        // properies needed for PaginatedCollection
        baseUrl:ASSIGNMENT_API,
        limit: 20,
        filter_options : {"course__course_id":1}, //暂时写死course_id为1
        sort_field : "-ctime",

        maybeFetch : function(options){
            if(this._fetched){
                options.success && options.success();
                return;
            }
            var self = this;
            var successWrapper = function(success){
                return function(){
                    self._fetched = true;
                    success && success.apply(this,arguments);
                };
            };
            options.success = successWrapper(options.success);
            this.fetch(options);
        }
    });



    window.CalendarEvent = Backbone.Model.extend({
        urlRoot: CALENDAREVENT_API,
        idAttribute:'post_id'
    });

    window.EventList = PaginatedCollection.extend({
        model: CalendarEvent,
        urlRoot: CALENDAREVENT_API,

        // properies needed for PaginatedCollection
        baseUrl:CALENDAREVENT_API,
        limit: 20,
        filter_options : {"course__course_id":1},
        sort_field : "event_date",

        maybeFetch : function(options){
            if(this._fetched){
                options.success && options.success();
                return;
            }
            var self = this;
            var successWrapper = function(success){
                return function(){
                    self._fetched = true;
                    success && success.apply(this,arguments);
                };
            };
            options.success = successWrapper(options.success);
            this.fetch(options);
        }
    });

    window.CalendarEventView = Backbone.View.extend({
        tagName: 'div',
        className: 'unit-block',

//        events: {
//            'click .delete': 'clear',
//            'click .add-comment' :'addComment'
//        },

        initialize: function(){
            this.model.bind('change', this.render, this);
            //this.model.bind('destroy', this.remove, this);
        },

        render: function(){
            var attributes = this.model.toJSON();
            var type = attributes.type;

            var newWeek = false;

            var date = attributes.event_date;
            var parsed_date = Date.parse(date);
            var week =  parsed_date.getWeek();

            //check whether it's belong to the same week of the previous one
            if(week!=window.last_event_week){
                //append the week days range.
                //$(".unit-info").css("borderTop","none");
                newWeek = true;
                $("<h3 class='week'>"+ parsed_date.moveToDayOfWeek(0,-1).toString("yyyy/MM/dd") +" - " + parsed_date.moveToDayOfWeek(6).toString("yyyy/MM/dd") + "</h3>").appendTo(this.el);
            }

            switch(type){
                //作业相关事件
                case "assignment":
                    $(this.el).append(newWeek? $(ich.AssignmentEvent(attributes)).css("borderTop","none"):  $(ich.AssignmentEvent(attributes)));
                    break;
                //考试相关事件
                case "exam":
                    $(this.el).append(newWeek? $(ich.ExamEvent(attributes)).css("borderTop","none"):  $(ich.ExamEvent(attributes)));
                    break;
                //会议相关事件
                case "meeting":
                    $(this.el).append(newWeek? $(ich.MeetingEvent(attributes)).css("borderTop","none"):  $(ich.MeetingEvent(attributes)));
                    break;
                default:
                    alert("系统不稳定，请和管理员联系");
            }

            window.last_event_week = week;
            return this;
        }
    });


    window.EventListView = Backbone.View.extend({
        initialize: function(){
            _.bindAll(this, 'addOne', 'appendOne','addAll');

            this.collection.bind('add', this.addOne);
            this.collection.bind('reset', this.addAll, this);
            //$(window).bind('scroll', this.collection, this.loadMore);
            this.views = [];
        },

        addAll: function(){
            window.last_event_week = null;
            this.views = [];  //view.remove()清除
            this.collection.each(this.appendOne);
        },

        //used when fetch old posts
        appendOne: function(calendarEvent){
            var view = new CalendarEventView({
                model: calendarEvent
            });
            $(this.el).append(view.render().el);
            this.views.push(view);
            view.bind('all', this.rethrow, this);
        },

        //used when create new post
        addOne: function(calendarEvent){
            var view = new CalendarEventView({
                model: calendarEvent
            });
            $(this.el).prepend(view.render().el);
            this.views.push(view);
            view.bind('all', this.rethrow, this);
        },

        rethrow: function(){
            this.trigger.apply(this, arguments);
        }
    });

    window.CreateEventEditor = Backbone.View.extend({
        events:{
            'click .assignment-opt': 'showAssignmentPanel',
            'click .assignment-panel .cancel' : 'hideAssignmentPanel',
            'click .exam-opt': 'showExamPanel',
            'click .exam-panel .cancel' : 'hideExamPanel',
            'click .meeting-opt': 'showMeetingPanel',
            'click .meeting-panel .cancel' : 'hideMeetingPanel',
            'click .assignment-panel .save': 'createAssignment',
            'click .exam-panel .save': 'createExam',
            'click .meeting-panel .save': 'createMeeting'
        },

        showAssignmentPanel: function(){
            $(".type-selector").hide();
            $(".assignment-panel").slideDown();
        },

        hideAssignmentPanel: function(){
            $(".assignment-panel").slideUp();
            $(".type-selector").show();
        },

        showExamPanel: function(){
            $(".type-selector").hide();
            $(".exam-panel").slideDown();
        },

        hideExamPanel: function(){
            $(".exam-panel").slideUp();
            $(".type-selector").show();
        },

        showMeetingPanel: function(){
            $(".type-selector").hide();
            $(".meeting-panel").slideDown();
        },

        hideMeetingPanel: function(){
            $(".meeting-panel").slideUp();
            $(".type-selector").show();
        },

        createAssignment: function(){
            alert("创建作业");
        },

        createExam: function(){
            alert("创建考试");
        },

        createMeeting: function(){
            alert("创建会议");
        },
//        createPost: function(){
//            //var content = this.$('#textContent').val();
//            var content = this.$('#textContent').editorBox("get_code");
//            if(content){
//                this.collection.create({
//                    school: SCHOOL,
//                    course: COURSE,
//                    creator: PARTICIPANT,
//                    content: content,
//                    comment_count : 0,
//                    isdeleted : false
//                },{ wait: true});
//                this.$('#textContent').val('');
//            }
//        },

        render: function(){
            $(this.el).html(ich.EventEditor());
        }
    });

    $(function(){
        var eventList = new EventList();

        var newEditor = new CreateEventEditor({
            el:$(".add_operation")
        });
        newEditor.render();

        var eventListView = new EventListView({
            el:$(".calendar-list"),
            collection: eventList
        });

        eventList.maybeFetch({
            success: _.bind(eventListView.render, eventListView)
            //data: {"course__course_id":1,"order_by":"-ctime"}
        });
    });
})();