/**
 * Created by PyCharm.
 * User: Administrator
 * Date: 12-5-28
 * Time: 上午11:52
 * To change this template use File | Settings | File Templates.
 */
(function(){

    window.Assignment = Backbone.Model.extend({
        urlRoot: ASSIGNMENT_API,
        idAttribute:'assignment_id'
    });

    window.AssignmentList = PaginatedCollection.extend({
        model: Assignment,
        urlRoot: ASSIGNMENT_API,

        // properies needed for PaginatedCollection
        baseUrl:ASSIGNMENT_API,
        limit: 20,
        filter_options : {"course__course_id":1}, //暂时写死course_id为1
        sort_field : "-ctime",

        maybeFetch : function(options){
            if(this._fetched){
                options.success && options.success();
                return;
            }
            var self = this;
            var successWrapper = function(success){
                return function(){
                    self._fetched = true;
                    success && success.apply(this,arguments);
                };
            };
            options.success = successWrapper(options.success);
            this.fetch(options);
        }
    });

    window.AssignmentView = Backbone.View.extend({
        tagName:"div",
        className:"row",
        events:{
            'click' : "showSubmission"
        },

        render: function(){
            $(this.el).html(ich.AssignmentTemplate(this.model.toJSON()));
            return this;
        },

        showSubmission: function(){
          window.location +=  "/" +this.model.toJSON().assignment_id + "/submissions";
        }
    });

    window.AssignmentListView = Backbone.View.extend({
        initialize: function(){
            _.bindAll(this, 'addOne', 'appendOne','addAll');

            this.collection.bind('add', this.addOne);
            this.collection.bind('reset', this.addAll, this);
            //$(window).bind('scroll', this.collection, this.loadMore);
            this.views = [];
        },

        addAll: function(){
            this.views = [];  //view.remove()清除
            this.collection.each(this.appendOne);
        },

        appendOne: function(assignment){
            var view = new AssignmentView({
                model: assignment
            });
            $(this.el).append(view.render().el);
            this.views.push(view);
            view.bind('all', this.rethrow, this);
        },

        addOne: function(assignment){
            var view = new AssignmentView({
                model: assignment
            });
            $(this.el).prepend(view.render().el);
            this.views.push(view);
            view.bind('all', this.rethrow, this);
        },

        rethrow: function(){
            this.trigger.apply(this, arguments);
        }
    });

    window.NewAssignmentPopup = Backbone.View.extend({

        events:{
            'click .close': 'closePopup',
            'click .cancel': 'closePopup',
            'click .save' : 'save'
        },

        initialize:function(){
            _.bindAll(this, 'closePopup','save');
        },

        showPopup: function(){
            var winW = $(window).width();
            var winH = $(window).height();
            var sroT = $(window).scrollTop();
            var sroL = $(window).scrollLeft();
            var objW = $(".popup").width();
            var objH = $(".popup").height();
            var left = (winW - objW) / 2 + sroL;
            var top = (winH - objH) / 2 + sroT;
            $(".popup").css("left",left).css("top",top).fadeIn(300);
        },

        closePopup: function(){
            $(this.el).hide();
        },

        save : function(){
            var title = $("input[name='title']").val();
            //暂省略截止日期
            //暂省略提交类型
            var category = $("select[name='category']").val();
            var max_grade = $("input[name='max-grade']").val();
            var description = this.$('#description').editorBox("get_code");
            //暂省略相关资源
            //暂省略事件发布时间
            var today = new Date();
            var day = today.getDate();
            var month = today.getMonth() + 1;
            var year = today.getFullYear();
            var hour = today.getHours();
            var minute = today.getMinutes();
            var second = today.getSeconds();

            var due_date= year + "-" + month + "-" + day;
            var due_time= hour + ":" + minute + ":" + second;
            //alert(due_date);
            //暂省略学生通知选项

            //alert("保存 "+title + " " +category + " " + max_grade + " " + description);
            this.collection.create({
                school: SCHOOL,
                course: COURSE,
                creator: PARTICIPANT,
                title:title,
                description: description,
                due_date:due_date,
                due_time:due_time,
                max_grade: max_grade,
                category:category,
                isdeleted : false
            },{ wait: true});
            $(this.el).hide();
        },

        render: function(){
            $(this.el).html(ich.AssignmentPopup());
        }


    });

    $(function(){
        var assignment_list = new AssignmentList();

        var assignment_list_view = new AssignmentListView({
            el:$(".assignment_list"),
            collection: assignment_list
        });

        assignment_list.maybeFetch({
            success: _.bind(assignment_list_view.render, assignment_list_view)
            //data: {"course__course_id":1,"order_by":"-ctime"}
        });

        //render the add submission popup panel， but don't show it yet
        var add_submission_popup = new NewAssignmentPopup({
            el: $(".popup"),
            collection: assignment_list
        });
        add_submission_popup.render();

        $(".addSubmission").click(function(){
            add_submission_popup.showPopup();
        });



        require([
            'tinymce.editor_box'
        ], function() {
            $("#description").editorBox();
        });
    });
})();