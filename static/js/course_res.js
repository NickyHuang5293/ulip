/**
 * Created by PyCharm.
 * User: Administrator
 * Date: 12-5-3
 * Time: 下午9:10
 * To change this template use File | Settings | File Templates.
 */
(function(){
    /****************************************BOOK RESOURCE START********************************/

    //define model "ResBook"
    window.ResBook = Backbone.Model.extend({
        urlRoot: RES_BOOK_API,
        idAttribute:'book_id'
    });

    window.BookCollection = Backbone.Collection.extend({
        model: ResBook,
        urlRoot: RES_BOOK_API,

        maybeFetch : function(options){
            if(this._fetched){
                options.success && options.success();
                return;
            }
            var self = this;
            var successWrapper = function(success){
                return function(){
                    self._fetched = true;
                    success && success.apply(this,arguments);
                };
            };
            options.success = successWrapper(options.success);
            this.fetch(options);
        }

    });

    window.BookUnitView = Backbone.View.extend({
        tagName: 'div',
        className: 'unit-block',

        events: {
            'click .delete-book': 'clear',
            'click .edit-book': 'edit',
            'click .edit-book-panel .save-modify': 'save_modify',
            'click .edit-book-panel .cancel-modify': 'cancel_modify',
            'click .unit-info, .book-detail-panel .book-slideup': 'ctrlDetailPanel'
        },

        initialize: function(){
            this.model.bind('change', this.render, this);
            this.model.bind('destroy', this.remove, this);

            $(this.el).data("detail_state","hide");
        },

        clear: function(e){
            e.preventDefault();
           this.model.destroy({
               success: function(){ },
               error: function(){ alert("delete failed!"); },
               wait:true
           });
        },

        showBookPanel: function(){
            this.$(".unit-info").hide();
            this.$(".edit-book-panel").slideDown();
        },

        hideBookPanel: function(){
            this.$(".edit-book-panel").slideUp();
            this.$(".unit-info").show();
        },

        edit: function(e){
            e.preventDefault();
            //init editor value
            if(this.model.get('isRequired'))
                this.$('.edit-book-panel .res-required #book-required').attr("checked","checked");
            else
                this.$('.edit-book-panel .res-required #book-supplemental').attr("checked","checked");

            this.$('.edit-book-panel .res-book-info #title').val(this.model.get('book_title'));
            this.$('.edit-book-panel .res-book-info #author').val(this.model.get('book_author'));
            this.$('.edit-book-panel .res-desc #textContent').val(this.model.get('description'));

            if($(this.el).data("detail_state")=="show"){
                this.hideDetailPanel(e);
                $(this.el).data("detail_state","hide");
            }
            this.showBookPanel();
            e.stopPropagation();
        },

        save_modify: function(){
            var obj = {};
            obj['isRequired'] = this.$('#book-required').prop('checked');
            obj['book_title'] = this.$('#title').val();
            obj['book_author'] = this.$('#author').val();
            obj['description'] = this.$('#textContent').val();
            this.model.save(obj);

            this.hideBookPanel();
        },

        cancel_modify: function(){
            this.hideBookPanel();
        },

        ctrlDetailPanel: function(e){
            e.preventDefault();

            if($(this.el).data("detail_state")=="hide"){
                this.showDetailPanel();
                $(this.el).data("detail_state","show");
            }
            else{
                this.hideDetailPanel();
                $(this.el).data("detail_state","hide");
            }
        },

        showDetailPanel: function(){
            this.$(".book-detail-panel").slideDown();
        },

        hideDetailPanel:function(){
            this.$(".book-detail-panel").slideUp();
        },

        render: function(){
            var data = this.model.toJSON();
            data.description = textToPageEncode(data.description)
            $(this.el).html(ich.BookUnit(this.model.toJSON()));
            this.$('.book-detail-panel .res-desc-content').html(data.description);
            return this;
        }
    });

    window.BookListView = Backbone.View.extend({

        initialize: function(){
            _.bindAll(this, 'addOne', 'addAll');

            this.collection.bind('add', this.addOne);
            this.collection.bind('reset', this.addAll, this);
            this.views = [];
        },

        addAll: function(){
            this.views = [];
            this.collection.each(this.addOne);
        },

        addOne: function(resbook){

            var view = new BookUnitView({
                model: resbook
            });
            $(this.el).prepend(view.render().el);
            this.views.push(view);
            view.bind('all', this.rethrow, this);
        },

        rethrow: function(){
            this.trigger.apply(this, arguments);
        }
    });
    /****************************************BOOK RESOURCE END********************************/

    /****************************************LINK RESOURCE START********************************/
    //define model "ResLink"
    window.ResLink = Backbone.Model.extend({
        urlRoot: RES_LINK_API,
        idAttribute:'link_id'
    });

    window.LinkCollection = Backbone.Collection.extend({
        model: ResLink,
        urlRoot: RES_LINK_API,

         maybeFetch : function(options){
            if(this._fetched){
                options.success && options.success();
                return;
            }
            var self = this;
            var successWrapper = function(success){
                return function(){
                    self._fetched = true;
                    success && success.apply(this,arguments);
                };
            };
            options.success = successWrapper(options.success);
            this.fetch(options);
         }
    });

    window.LinkUnitView = Backbone.View.extend({
        tagName: 'div',
        className: 'unit-block',

        events: {
            'click .delete-link': 'clear',
            'click .edit-link': 'edit',
            'click .unit-info .name': 'view_link',
            'click .edit-link-panel .save-modify': 'save_modify',
            'click .edit-link-panel .cancel-modify': 'cancel_modify',
            'click .unit-info, .link-detail-panel .link-slideup': 'ctrlDetailPanel'
        },

        initialize: function(){
            this.model.bind('change', this.render, this);
            this.model.bind('destroy', this.remove, this);

            $(this.el).data("detail_state","hide");
        },

        clear: function(e){
            e.preventDefault();
            this.model.destroy({
                success: function(){ },
                error: function(){ alert("delete failed!"); },
                wait:true
            });
        },

        view_link: function(e){
            e.stopPropagation();
        },

        showLinkPanel: function(){
            this.$(".unit-info").hide();
            this.$(".edit-link-panel").slideDown();
        },

        hideLinkPanel: function(){
            this.$(".edit-link-panel").slideUp();
            this.$(".unit-info").show();
        },

        edit: function(e){
            e.preventDefault();
            //init editor value
            if(this.model.get('isRequired'))
                this.$('.edit-link-panel .res-required #link-required').attr("checked","checked");
            else
                this.$('.edit-link-panel .res-required #link-supplemental').attr("checked","checked");

            this.$('.edit-link-panel .res-link-info #url').val(this.model.get('link_url'));
            this.$('.edit-link-panel .res-link-info #name').val(this.model.get('link_name'));
            this.$('.edit-link-panel .res-desc #textContent').val(this.model.get('description'));

            if($(this.el).data("detail_state")=="show"){
                this.hideDetailPanel(e);
                $(this.el).data("detail_state","hide");
            }
            this.showLinkPanel();
            e.stopPropagation();
        },

        save_modify: function(){
            var obj = {};
            obj['isRequired'] = this.$('#required').prop('checked');
            obj['link_url'] = this.$('#url').val();
            obj['link_name'] = this.$('#name').val();
            obj['description'] = this.$('#textContent').val();
            this.model.save(obj);
            this.hideLinkPanel();
        },

        cancel_modify: function(){
            this.hideLinkPanel();
        },

        ctrlDetailPanel: function(e){
            e.preventDefault();

            if($(this.el).data("detail_state")=="hide"){
                this.showDetailPanel();
                $(this.el).data("detail_state","show");
            }
            else{
                this.hideDetailPanel(e);
                $(this.el).data("detail_state","hide");
            }
        },

        showDetailPanel: function(){
            this.$(".link-detail-panel").slideDown();
        },

        hideDetailPanel:function(e){
            this.$(".link-detail-panel").slideUp();
        },

       render: function(){
           var data = this.model.toJSON();
           data.description = textToPageEncode(data.description);

           $(this.el).html(ich.LinkUnit(this.model.toJSON()));
           this.$('.unit-info .link-info .description').html(data.description);
           return this;
       }
   });

    window.LinkListView = Backbone.View.extend({

        initialize: function(){
            _.bindAll(this, 'addOne', 'addAll');

            this.collection.bind('add', this.addOne);
            this.collection.bind('reset', this.addAll, this);
            this.views = [];
        },

        addAll: function(){
            this.views = [];
            this.collection.each(this.addOne);
        },

        addOne: function(reslink){

            var view = new LinkUnitView({
                model: reslink
            });
            $(this.el).prepend(view.render().el);
            this.views.push(view);
            view.bind('all', this.rethrow, this);
        },

        rethrow: function(){
            this.trigger.apply(this, arguments);
        }
    });
    /****************************************LINK RESOURCE END********************************/
    window.ResEditorView = Backbone.View.extend({
        events:{
            'click .book-opt': 'showBookPanel',
            'click .new-book-panel .cancel' : 'hideBookPanel',
            'click .new-book-panel .save': 'createResBook',

            'click .link-opt': 'showLinkPanel',
            'click .new-link-panel .cancel' : 'hideLinkPanel',
            'click .new-link-panel .save': 'createResLink'
        },

        showBookPanel: function(){
            this.resetBook();
            $(".type-selector").hide();
            $(".new-book-panel").slideDown();
        },

        hideBookPanel: function(){
            $(".new-book-panel").slideUp();
            $(".type-selector").show();
        },

        createResBook: function(){
            var bRequired = this.$('.new-book-panel #book-required').prop('checked');
            var title = this.$('.new-book-panel #title').val();
            var author = this.$('.new-book-panel #author').val();
            var desc = this.$('.new-book-panel #textContent').val();

            if(title){
                post_list_app.book_collection.create({
                    school: SCHOOL,
                    course: COURSE,
                    creator: PARTICIPANT,
                    isdeleted : false,
                    isRequired: bRequired,
                    book_title: title,
                    book_author: author,
                    description: desc

                },{ wait: true});

                this.hideBookPanel();
            }
        },

        resetBook: function(){
            this.$('.new-book-panel .res-required #book-required').attr("checked","checked");
            this.$('.new-book-panel #title').val("书名");
            this.$('.new-book-panel #author').val("作者");
            this.$('.new-book-panel #textContent').val("");
        },

        showLinkPanel: function(){
            this.resetLink();
            $(".type-selector").hide();
            $(".new-link-panel").slideDown();
        },

        hideLinkPanel: function(){
            $(".new-link-panel").slideUp();
            $(".type-selector").show();
        },

        createResLink: function(){

            var bRequired = this.$('.new-link-panel #link-required').prop('checked');
            var url = this.$('.new-link-panel #url').val();
            var name = this.$('.new-link-panel #name').val();
            var desc = this.$('.new-link-panel #textContent').val();

            if(url && name){
                post_list_app.link_collection.create({
                    school: SCHOOL,
                    course: COURSE,
                    creator: PARTICIPANT,
                    isdeleted : false,
                    isRequired: bRequired,
                    link_url: url,
                    link_name: name,
                    description: desc

                },{ wait: true});

                this.hideLinkPanel();
            }
        },

        resetLink: function(){
            this.$('.new-link-panel .res-required #link-required').attr("checked","checked");
            this.$('.new-link-panel #url').val("");
            this.$('.new-link-panel #name').val("");
            this.$('.new-link-panel #textContent').val("");
        },

        render: function(){
            $(this.el).html(ich.ResEditor());
        }
    });


    $(function(){
        window.post_list_app =   window.post_list_app || {};

        post_list_app.book_collection = new BookCollection();
        post_list_app.link_collection = new LinkCollection();

        //initialize the post list view
        post_list_app.booklist = new BookListView({
            el:$(".resbook-list"),
            collection: post_list_app.book_collection
        });
        post_list_app.linklist = new LinkListView({
             el:$(".reslink-list"),
             collection: post_list_app.link_collection
         });

        post_list_app.book_collection.maybeFetch({
            success: _.bind(post_list_app.booklist.render, post_list_app.booklist)
        });

        post_list_app.link_collection.maybeFetch({
            success: _.bind(post_list_app.linklist.render, post_list_app.linklist)
        });

        var newEditor = new ResEditorView({
            el:$(".add_operation")
        });
        newEditor.render();

    });
})();

