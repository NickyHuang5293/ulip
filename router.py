__author__ = 'bisonchen'

class AppBasedRouter(object):
    def db_for_read(self, model, **hints):
        if model._meta.app_label in [ 'auth', 'contenttypes', 'sessions',]:
            return 'auth-session'
        if model._meta.app_label == 'baseinfo':
            return 'baseinfo'
        if model._meta.app_label == 'campaign':
            return 'campaign'
        return None

    def db_for_write(self, model, **hints):
        if model._meta.app_label in [ 'auth', 'contenttypes', 'sessions',]:
            return 'auth-session'
        if model._meta.app_label == 'baseinfo':
            return 'baseinfo'
        if model._meta.app_label == 'campaign':
            return 'campaign'
        return None

    def allow_relation(self, value, instance):
        return True;