# Create your views here.
from django.views.generic.base import TemplateView
from django.shortcuts import get_object_or_404
from campaign.models import Assignment

class PostListView(TemplateView):

    template_name = 'discussion/post_list.html'

    def get(self, request, *args, **kwargs):
        # set the session before render
        self.request.session['school'] =  "/api/v1/school/1/",
        self.request.session['course'] = '/api/v1/course/1/',
        self.request.session['participant'] = '/api/v1/participant/2/',
        return super(PostListView,self).get(self, request, *args, **kwargs)


class ResListView(TemplateView):

	template_name = 'resources/resources.html'

	def get(self, request, *args, **kwargs):
		# set the session before render
		self.request.session['school'] =  "/api/v1/school/1/",
		self.request.session['course'] = '/api/v1/course/1/',
		self.request.session['participant'] = '/api/v1/participant/2/',
		return super(ResListView,self).get(self, request, *args, **kwargs)

class AssignmentListView(TemplateView):
    template_name = 'assignment/assignment.html'

    def get(self, request, *args, **kwargs):
    # set the session before render
        self.request.session['school'] =  "/api/v1/school/1/",
        self.request.session['course'] = '/api/v1/course/1/',
        self.request.session['participant'] = '/api/v1/participant/1/',
        return super(AssignmentListView,self).get(self, request, *args, **kwargs)


class SubmissionListView(TemplateView):
    template_name = 'assignment/submission.html'

    def get_context_data(self, **kwargs):
        context = super(SubmissionListView, self).get_context_data(**kwargs)
        self.assignment = get_object_or_404(Assignment, assignment_id= self.kwargs["assignment_id"]);
        context["assignment"] = self.assignment
        return context

    def get(self, request, *args, **kwargs):
    # set the session before render
        self.request.session['school'] =  "/api/v1/school/1/",
        self.request.session['course'] = '/api/v1/course/1/',
        self.request.session['participant'] = '/api/v1/participant/1/',
        return super(SubmissionListView,self).get(self, request, *args, **kwargs)

class CalendarEventListView(TemplateView):
    template_name = 'calendar/calendar.html'

    def get(self, request, *args, **kwargs):
        # set the session before render
        self.request.session['school'] =  "/api/v1/school/1/",
        self.request.session['course'] = '/api/v1/course/1/',
        self.request.session['participant'] = '/api/v1/participant/1/',
        return super(CalendarEventListView,self).get(self, request, *args, **kwargs)

class SyllabusListView(TemplateView):
		template_name = 'syllabus/syllabus.html'

		def get(self, request, *args, **kwargs):
			# set the session before render
			self.request.session['school'] =  "/api/v1/school/1/",
			self.request.session['course'] = '/api/v1/course/1/',
			self.request.session['participant'] = '/api/v1/participant/1/',
			return super(SyllabusListView,self).get(self, request, *args, **kwargs)
