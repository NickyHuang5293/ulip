__author__ = 'bisonchen'
from tastypie.api import Api
from resources import *
from baseinfo.api.resources import *

v1 = Api("v1")
v1.register(PostResource())
v1.register(CommentResource())

v1.register(ResBookResource())
v1.register(ResLinkResource())
v1.register(ResFileResource())

v1.register(AssignmentResource())
v1.register(ExamResource())
v1.register(MeetingResource())
v1.register(CalendarEventResource())
v1.register(SyllabusResource())

v1.register(SchoolResource())
v1.register(ParticipantResource())
v1.register(CourseResource())