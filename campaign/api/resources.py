from Tkconstants import ALL
from tastypie.constants import ALL_WITH_RELATIONS
from tastypie.paginator import Paginator

__author__ = 'bisonchen'
from tastypie.resources import ModelResource
from tastypie.authorization import Authorization
from tastypie import fields
from baseinfo.api.resources import  *
from campaign.models import *
import datetime

class PostResource(ModelResource):
    school = fields.ForeignKey(SchoolResource,'school')
    course = fields.ForeignKey(CourseResource,'course')
    creator = fields.ForeignKey(ParticipantResource,'creator',full=True)
    comments = fields.ToManyField('campaign.api.resources.CommentResource','comment_set', null=True)

    class Meta:
        queryset = Post.objects.all()
        authorization = Authorization()
        list_allowed_methods = ['get', 'post','put','delete']
        detail_allowed_methods = ['get', 'post','put','delete']
        paginator_class = Paginator
        ordering = ['ctime']
        filtering = {
            "post_id": ('exact',),
            "isdeleted":ALL,
            "course":ALL_WITH_RELATIONS,
            "creator":ALL_WITH_RELATIONS,
            "school":ALL_WITH_RELATIONS
        }

    def dehydrate_ctime(self, bundle):
        return bundle.data['ctime'].strftime('%Y-%m-%d %H:%M:%S')

#    def hydrate_content(self, bundle):
#        bundle.data['content'] = bundle.data['content'].upper()
#        return bundle

class CommentResource(ModelResource):
    school = fields.ForeignKey(SchoolResource,'school')
    course = fields.ForeignKey(CourseResource,'course')
    creator = fields.ForeignKey(ParticipantResource,'creator',full=True)
    post = fields.ToOneField(PostResource,'post')

    class Meta:
        queryset = Comment.objects.all()
        authorization = Authorization()
        list_allowed_methods = ['get', 'post','put','delete']
        detail_allowed_methods = ['get', 'post','put','delete']
        filtering = {
            "isdeleted":ALL,
            "course":ALL_WITH_RELATIONS,
            "creator":ALL_WITH_RELATIONS,
            "school":ALL_WITH_RELATIONS,
            'post':ALL_WITH_RELATIONS
        }

    def dehydrate_ctime(self, bundle):
        return bundle.data['ctime'].strftime('%Y-%m-%d %H:%M:%S')


class ResBookResource(ModelResource):
	school = fields.ForeignKey(SchoolResource,'school')
	course = fields.ForeignKey(CourseResource,'course')
	creator = fields.ForeignKey(ParticipantResource,'creator',full=True)

	class Meta:
		queryset = ResBook.objects.all()
		authorization = Authorization()
		list_allowed_methods = ['get', 'post','put','delete']
		detail_allowed_methods = ['get', 'post','put','delete']
		
		filtering = {
			"book_id": ('exact',),
			"isdeleted":ALL,
			"course":ALL_WITH_RELATIONS,
			"creator":ALL_WITH_RELATIONS,
			"school":ALL_WITH_RELATIONS
		}

	def dehydrate_ctime(self, bundle):
		return bundle.data['ctime'].strftime('%Y-%m-%d %H:%M:%S')

class ResLinkResource(ModelResource):
	school = fields.ForeignKey(SchoolResource,'school')
	course = fields.ForeignKey(CourseResource,'course')
	creator = fields.ForeignKey(ParticipantResource,'creator',full=True)

	class Meta:
		queryset = ResLink.objects.all()
		authorization = Authorization()
		list_allowed_methods = ['get', 'post','put','delete']
		detail_allowed_methods = ['get', 'post','put','delete']
		filtering = {
			"link_id": ('exact',),
			"isdeleted":ALL,
			"course":ALL_WITH_RELATIONS,
			"creator":ALL_WITH_RELATIONS,
			"school":ALL_WITH_RELATIONS
		}

	def dehydrate_ctime(self, bundle):
		return bundle.data['ctime'].strftime('%Y-%m-%d %H:%M:%S')

class ResFileResource(ModelResource):
	school = fields.ForeignKey(SchoolResource,'school')
	course = fields.ForeignKey(CourseResource,'course')
	creator = fields.ForeignKey(ParticipantResource,'creator',full=True)

	class Meta:
		queryset = ResFile.objects.all()
		authorization = Authorization()
		list_allowed_methods = ['get', 'post','put','delete']
		detail_allowed_methods = ['get', 'post','put','delete']
		filtering = {
			"file_id": ('exact',),
			"isdeleted":ALL,
			"course":ALL_WITH_RELATIONS,
			"creator":ALL_WITH_RELATIONS,
			"school":ALL_WITH_RELATIONS
		}

	def dehydrate_ctime(self, bundle):
		return bundle.data['ctime'].strftime('%Y-%m-%d %H:%M:%S')


class AssignmentResource(ModelResource):
    school = fields.ForeignKey(SchoolResource,'school')
    course = fields.ForeignKey(CourseResource,'course')
    creator = fields.ForeignKey(ParticipantResource,'creator')
    # comments = fields.ToManyField('campaign.api.resources.CommentResource','comment_set', null=True)

    class Meta:
        queryset = Assignment.objects.all()
        authorization = Authorization()
        list_allowed_methods = ['get', 'post','put','delete']
        detail_allowed_methods = ['get', 'post','put','delete']
        paginator_class = Paginator
        ordering = ['ctime']
        filtering = {
            "assignment_id": ('exact',),
            "isdeleted":ALL,
            "course":ALL_WITH_RELATIONS,
            "creator":ALL_WITH_RELATIONS,
            "school":ALL_WITH_RELATIONS
        }

#        def hydrate_due_date(self, bundle):
#            bundle.data['due_date'] = datetime.date.today();
#            return bundle
#
#        def hydrate_due_time(self, bundle):
#            bundle.data['due_time'] = datetime.time(12)
#            return bundle

class ExamResource(ModelResource):
    school = fields.ForeignKey(SchoolResource,'school')
    course = fields.ForeignKey(CourseResource,'course')
    creator = fields.ForeignKey(ParticipantResource,'creator')

    class Meta:
        queryset = Exam.objects.all()
        authorization = Authorization()
        list_allowed_methods = ['get', 'post','put','delete']
        detail_allowed_methods = ['get', 'post','put','delete']
        paginator_class = Paginator
        ordering = ['ctime']
        filtering = {
            "exam_id": ('exact',),
            "isdeleted":ALL,
            "course":ALL_WITH_RELATIONS,
            "creator":ALL_WITH_RELATIONS,
            "school":ALL_WITH_RELATIONS
        }

class MeetingResource(ModelResource):
    school = fields.ForeignKey(SchoolResource,'school')
    course = fields.ForeignKey(CourseResource,'course')
    creator = fields.ForeignKey(ParticipantResource,'creator')

    class Meta:
        queryset = Meeting.objects.all()
        authorization = Authorization()
        list_allowed_methods = ['get', 'post','put','delete']
        detail_allowed_methods = ['get', 'post','put','delete']
        paginator_class = Paginator
        ordering = ['ctime']
        filtering = {
            "meeting_id": ('exact',),
            "isdeleted":ALL,
            "course":ALL_WITH_RELATIONS,
            "creator":ALL_WITH_RELATIONS,
            "school":ALL_WITH_RELATIONS
        }

class CalendarEventResource(ModelResource):
    school = fields.ForeignKey(SchoolResource,'school')
    course = fields.ForeignKey(CourseResource,'course')
    creator = fields.ForeignKey(ParticipantResource,'creator')
    assignment = fields.ForeignKey(AssignmentResource,'assignment',full=True, null=True)
    exam = fields.ForeignKey(ExamResource,'exam',full=True,null=True)
    meeting = fields.ForeignKey(MeetingResource,'meeting', full=True, null=True)

    class Meta:
        queryset = CalendarEvent.objects.all()
        authorization = Authorization()
        list_allowed_methods = ['get', 'post','put','delete']
        detail_allowed_methods = ['get', 'post','put','delete']
        paginator_class = Paginator
        ordering = ['event_date']
        filtering = {
            "meeting_id": ('exact',),
            "isdeleted":ALL,
            "course":ALL_WITH_RELATIONS,
            "creator":ALL_WITH_RELATIONS,
            "school":ALL_WITH_RELATIONS,
            "assignment":ALL_WITH_RELATIONS,
            "exam":ALL_WITH_RELATIONS,
            "meeting":ALL_WITH_RELATIONS,
        }

    def dehydrate_event_date(self, bundle):
        return bundle.data['event_date'].strftime('%Y-%m-%d')

class SyllabusResource(ModelResource):
	school = fields.ForeignKey(SchoolResource,'school')
	course = fields.ForeignKey(CourseResource,'course')
	creator = fields.ForeignKey(ParticipantResource,'creator',full=True)

	class Meta:
		queryset = Syllabus.objects.all()
		authorization = Authorization()
		list_allowed_methods = ['get', 'post','put','delete']
		detail_allowed_methods = ['get', 'post','put','delete']
		filtering = {
			"syllabus_id": ('exact',),
			"isdeleted":ALL,
			"course":ALL_WITH_RELATIONS,
			"creator":ALL_WITH_RELATIONS,
			"school":ALL_WITH_RELATIONS
		}

	def dehydrate_ctime(self, bundle):
		return bundle.data['ctime'].strftime('%Y-%m-%d %H:%M:%S')