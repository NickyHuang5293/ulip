# -*- coding: utf-8 -*-
from django.db import models
from baseinfo.models import *
import datetime

ASSIGNMENT_CATEGORY=(
    (u'not graded',u'自测'),
    (u'general',u'普通作业'),
    (u'exam',u'考试'),
    (u'quizzes',u'小测验'),
)

MEETING_TYPE=(
    (u'lecture',u'讲座'),
    (u'exam review',u'考试复习'),
    (u'others',u'其他')
)

EVENT_TYPE=(
    (u'assignment',u'作业'),
    (u'exam',u'考试'),
    (u'meeting',u'会议')
)

class Post(models.Model):
    post_id = models.AutoField(primary_key=True)
    school = models.ForeignKey(School)
    course = models.ForeignKey(Course)
    creator = models.ForeignKey(Participant, verbose_name="发帖者")
    ctime = models.DateTimeField(default=datetime.datetime.now)
    content = models.TextField(max_length=1024)
    comment_count = models.IntegerField()
    isdeleted = models.BooleanField(default=False)

class Comment(models.Model):
    comment_id = models.AutoField(primary_key=True)
    school = models.ForeignKey(School)
    course = models.ForeignKey(Course)
    post = models.ForeignKey(Post)
    content = models.TextField(max_length=1024)
    creator = models.ForeignKey(Participant,related_name='commentor')
    ctime = models.DateTimeField(default=datetime.datetime.now)
    isdeleted = models.BooleanField(default=False)

class ResBook(models.Model):

	book_id = models.AutoField(primary_key=True)
	school = models.ForeignKey(School)
	course = models.ForeignKey(Course)
	creator = models.ForeignKey(Participant, verbose_name="发帖者")
	ctime = models.DateTimeField(default=datetime.datetime.now)
	isdeleted = models.BooleanField(default=False)
	isRequired = models.BooleanField(default=True)
	tags = models.CharField(max_length=512, null=True)

	book_title = models.CharField(max_length=128)
	book_author = models.CharField(max_length=64)
	description = models.TextField(max_length=1024)

class ResLink(models.Model):

	link_id = models.AutoField(primary_key=True)
	school = models.ForeignKey(School)
	course = models.ForeignKey(Course)
	creator = models.ForeignKey(Participant, verbose_name="发帖者")
	ctime = models.DateTimeField(default=datetime.datetime.now)
	isdeleted = models.BooleanField(default=False)
	isRequired = models.BooleanField(default=True)
	tags = models.CharField(max_length=512, null=True)

	link_url = models.CharField(max_length=512)
	link_name = models.CharField(max_length=64)
	description = models.TextField(max_length=1024)

class ResFile(models.Model):

	file_id = models.AutoField(primary_key=True)
	school = models.ForeignKey(School)
	course = models.ForeignKey(Course)
	creator = models.ForeignKey(Participant, verbose_name="发帖者")
	ctime = models.DateTimeField(default=datetime.datetime.now)
	isdeleted = models.BooleanField(default=False)
	isRequired = models.BooleanField(default=True)
	tags = models.CharField(max_length=512, null=True)

	file_path = models.CharField(max_length=256)
	file_name = models.CharField(max_length=64)
	description = models.TextField(max_length=1024)

#作业
class Assignment(models.Model):
    assignment_id = models.AutoField(primary_key=True)
    school = models.ForeignKey(School)
    course = models.ForeignKey(Course)
    creator = models.ForeignKey(Participant, verbose_name="发布作业人")
    title = models.CharField(max_length=50)
    due_date = models.DateField()
    due_time = models.TimeField()
    # 暂省略提交类型（文件、链接）
    category = models.CharField(max_length=20, choices=ASSIGNMENT_CATEGORY)
    max_grade = models.PositiveIntegerField();
    description = models.TextField(max_length=1024)
    # 暂省略相关资源
    # 暂省略事件发布时间
    ctime = models.DateTimeField(default=datetime.datetime.now)
    isdeleted = models.BooleanField(default=False)

class Exam(models.Model):
    exam_id = models.AutoField(primary_key=True)
    school = models.ForeignKey(School)
    course = models.ForeignKey(Course)
    creator = models.ForeignKey(Participant, verbose_name="发布人")
    title = models.CharField(max_length=50)
    date = models.DateField()
    start_time = models.TimeField()
    end_time = models.TimeField()
    location = models.CharField(max_length=80)
    description = models.TextField(max_length=1024)
    # 暂省略相关资源
    ctime = models.DateTimeField(default=datetime.datetime.now)
    isdeleted = models.BooleanField(default=False)

class Meeting(models.Model):
    meeting_id = models.AutoField(primary_key=True)
    school = models.ForeignKey(School)
    course = models.ForeignKey(Course)
    creator = models.ForeignKey(Participant, verbose_name="发布人")
    title = models.CharField(max_length=50)
    date = models.DateField()
    start_time = models.TimeField()
    end_time = models.TimeField()
    location = models.CharField(max_length=80)
    type =models.CharField(max_length=30, choices=MEETING_TYPE)
    description = models.TextField(max_length=1024)
    # 暂省略相关资源
    ctime = models.DateTimeField(default=datetime.datetime.now)
    isdeleted = models.BooleanField(default=False)

class CalendarEvent(models.Model):
    event_id = models.AutoField(primary_key=True)
    school = models.ForeignKey(School)
    course = models.ForeignKey(Course)
    creator = models.ForeignKey(Participant, verbose_name="发布人")
    type = models.CharField(max_length=30, choices=EVENT_TYPE)
    # visible_since 到指定日期时才在日历列表中显示
    visible_since = models.DateTimeField()
    # 事件时间（对于作业为截止时间;对于exam、meeting则为安排时间）
    event_date = models.DateField()
    assignment = models.ForeignKey(Assignment)
    exam = models.ForeignKey(Exam)
    meeting = models.ForeignKey(Meeting)
    ctime = models.DateTimeField(default=datetime.datetime.now)
    isdeleted = models.BooleanField(default=False)

class Syllabus(models.Model):

	syllabus_id = models.AutoField(primary_key=True)
	school = models.ForeignKey(School)
	course = models.ForeignKey(Course)
	creator = models.ForeignKey(Participant, verbose_name="发帖者")
	ctime = models.DateTimeField(default=datetime.datetime.now)
	isdeleted = models.BooleanField(default=False)

	title = models.CharField(max_length=128)
	content = models.CharField(max_length=2048)