from django.views.generic.base import TemplateView

__author__ = 'bisonchen'
from django.conf.urls.defaults import patterns, url, include

from views import PostListView, ResListView,  AssignmentListView, SubmissionListView, CalendarEventListView, SyllabusListView
from api import v1

urlpatterns = patterns('',
	url(r'^$', PostListView.as_view(), name='post_list'),

    url(r'^discussion$', PostListView.as_view(), name='post_list'),

	url(r'^resources$', ResListView.as_view(), name="resources"),

	url(r'^assignment$',AssignmentListView.as_view(), name='assignment_list'),

    url(r'^calendar$', CalendarEventListView.as_view(), name='event_list'),

	url(r'^syllabus$', SyllabusListView.as_view(), name='syllabus'),

    url(r'assignment/(?P<assignment_id>\d+)/submissions$', SubmissionListView.as_view(), name='submission_list'),

    url(r'^api/', include(v1.urls))
)

